import java.util.Scanner;

public class OperacionesCadenas {


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Digite dos cadenas para operar sobre ellas \n");
        String cadena1 = sc.nextLine();
        String cadena2 = sc.nextLine();
        System.out.println("Digite la operacion a realizar");
        System.out.println("1. Concatenacion");
        System.out.println("2. Potencia");
        System.out.println("3. Longitud");
        System.out.println("4. Reflexion o inversa");
        System.out.println("5. Subcadenas");
        System.out.println("6. Prefijos y sufijos");
        int opcion = sc.nextInt();

        switch (opcion) {
            case 1:
                System.out.println("Resultado de la concatenacion: " + concatenacion(cadena1, cadena2));
                break;
            case 2:
                System.out.println("Digite el valor de la potencia: ");
                int potencia = sc.nextInt();
                System.out.println("Resultado de la potencia: " + potenciaCadena(cadena1, potencia));
                break;
            case 3:
                System.out.println("Longitud de la cadena: " + longitudCadena(cadena1));
                break;
            case 4:
                System.out.println("Cadena invertida: " + invertirCadena(cadena1));
                break;
            case 5:
                System.out.println("La subcadena " + cadena2 + subCadena(cadena1, cadena2));
                break;
            case 6:
                System.out.println("La subcadena " + cadena2 + prefijoYSufijo(cadena1, cadena2));
                break;
            default:
                System.out.println("Error. Opcion invalida");
                break;
        }
    }

    public static String concatenacion(String cadena1, String cadena2) {
        return cadena1 + cadena2;
    }

    public static String potenciaCadena(String cadena, int potencia) {
        StringBuilder cadenaFinal = new StringBuilder();
        for (int i = 0; i < potencia; i++) {
            cadenaFinal.append(cadena);
        }
        return cadenaFinal.toString();
    }

    public static int longitudCadena(String cadena) {
        return cadena.length();
    }

    public static String invertirCadena(String cadena) {
        StringBuilder cadenaInvertida = new StringBuilder(cadena);
        return cadenaInvertida.reverse().toString();
    }

    public static String subCadena(String cadena, String subCadena) {
        return (cadena.contains(subCadena) ? " si esta en la cadena " + cadena : " no la esta en la cadena " + cadena);
    }

    public static String prefijoYSufijo(String cadena, String subCadena) {
        String resultado = "";
        if (cadena.startsWith(subCadena)) {
            resultado += " es un prefijo de " + cadena;
        }
        if (cadena.endsWith(subCadena)) {
            resultado += " es un sufijo de " + cadena;
        }
        return resultado;

    }
}
